@extends('layouts.default')
		
@section('title')
-- Create User --
@stop

@section('content')	
	
	<h1>Create New User</h1>
	<div class='form-group' style='width:500px'>
	{{ Form::open(array('route' => 'users.store', 'class'=>'.form-inline')) }}
	
	
	<div class='form-group'>
			
		{{ Form::text('username', '', array('class' => 'form-control', 'placeholder' => 'Username')) }}
	</div>
	
	<div class='form-group'>
	
		
		{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
	</div>
	
	<div>{{ Form::submit('Create User', array('class' => 'btn btn-primary')) }}</div>
	
	{{ Form::close() }}
	</div>

@stop