@extends('layouts.default')
		
@section('title')
-- User List --
@stop

@section('content')		
		<h1>All Users</h1>


		@if (count($users) == 0)
		
			<h1>Theres no one here!</h1>
		
		@else
		
			@foreach ($users as $user)
		
			<li>{{ link_to("/users/{$user->username}", $user->username) }}</li>
		
		@endforeach
		
		@endif

@stop