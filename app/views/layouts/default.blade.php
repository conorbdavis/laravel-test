<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
	<head>
		<title>@yield('title')</title>
		@yield('header')
		{{ HTML::style('css/bootstrap.css'); }}
	</head>
	<body>
		
		@yield('content')
		
	</body>
</html>
